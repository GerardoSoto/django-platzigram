"""Posts views."""

# Django
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView
from django.urls import reverse, reverse_lazy

# Forms
from posts.forms import PostForm

# Models
from posts.models import Post


class PostDetailView(ListView, LoginRequiredMixin):
    """ Return a post with his details """
    template_name = 'posts/post_detail.html'
#/    model = Post ########### <--------------------- ?????????????????????????????????????
    
    #slug_field = 'pk'
    #Aslug_url_kwarg = 'pk' # post_id <= posts/urls.py <str:[name]>

    #ordering = ('-created')
    queryset = Post.objects.all()
    context_object_name = 'post'
    
    """    ERROR ENCONTRADO    
    ERROR ENCONTRADO    
    ERROR ENCONTRADO    
    ERROR ENCONTRADO    
    ERROR ENCONTRADO    
    ERROR ENCONTRADO    :::::::::::::      
    # Creacion de una url con kwargs: from django.urls import reverse
    url = reverse('users:detail', kwargs={'username': request.user.username})
    return redirect(url)
    """



class PostsFeedListView(ListView, LoginRequiredMixin):
    """ Return all published posts"""
    model = Post #########  <-------------------------------- ????????????????????
    template_name = "posts/feed.html"
    # Query args
    paginate_by = 8
    ordering = ('-created')
    queryset = Post.objects.all()
    # Context name
    context_object_name = 'posts'

    

"""
@login_required
def create_post(request):
    #create new post view
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            #create a post - save
            form.save()
            return redirect('posts:feed')
    else:
        form = PostForm()

    return render(
        request = request,
        template_name = 'posts/new.html',
        context = {
            'form': form,
            'user': request.user,
            'profile': request.user.profile,
        }
    )
"""
class PostDetailView(LoginRequiredMixin, CreateView):
    """ Create a new post """
    template_name  = 'posts/new.html'
    form_class = PostForm
    success_url = reverse_lazy('posts:feed')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["user"] = self.request.user
        context["profile"] = self.request.user.profile
        return context
    