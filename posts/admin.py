"""Model registration POST"""

# Django
from django.contrib import admin

# Model
from posts.models import Post

# Register your models here.
@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    """Post Admin."""
    list_display = ('pk','profile','photo','title','created')
    list_display_links = ('pk', 'profile')
    list_editable = ('photo', 'title')

    search_fields = (
        'pk',
        'profile',
        'title'
    )

    list_filter = (
        'created',
        'modified'
    )

