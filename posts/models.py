"""Posts models."""

# Django
from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Post(models.Model):
    """Attributes of the post class"""
#Bring the foreing keys
#If the user is deleted, their posts will also be deleted [CASCADE]
    user = models.ForeignKey(User, on_delete=models.CASCADE)
#If the profile is deleted, theirposts will also be deleted [CASCADE]
    #profile = models.ForeingKey(Profile, on_delete = models.CASCADE)  <- Ok or:
#                               /Users/models/Profile
    profile = models.ForeignKey('users.Profile', on_delete=models.CASCADE)
#Attributes
    title = models.CharField(max_length=255)
    photo = models.ImageField(upload_to='posts/photos')
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)


    def __str__(self):
        """Return the title and username."""
        return '{} by @{}'.format(self.title,self.user.username)


    def image_tag(self):
        from django.utils.html import escape
        return u'<img src=%s />' % escape('posts/photos')


