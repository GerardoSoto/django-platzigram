"""Forms of posts"""

# Django
from django.forms import ModelForm

# Models
from posts.models import Post


class PostForm(ModelForm):
    """Post model form."""
    class Meta:
        """Form settings.
        Configuración de la clase general"""

        model = Post
        fields = ['user', 'profile', 'title', 'photo']

