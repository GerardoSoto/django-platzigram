""" Posts URLs. """

# Django
from django.urls import path
from django.views.generic import TemplateView

# Views
from posts import views

urlpatterns = [
    path(
        route='',
        view= views.PostsFeedListView.as_view(), 
        name='feed'
    ),
    path(
        route= 'new/',
        view= views.PostDetailView.as_view(), 
        name='create_post'
    ),
    path(
        route='posts/<int:pk>/', # <- Doc 2.2
        #route='posts/', # good
        view= views.PostDetailView.as_view(),
        name='post_detail'
    ),
    path(
        route='posts/trendingtopic',
        view=TemplateView.as_view(
            template_name='posts/trendingtopic.html'),
        name='trendingtopic'
    ),
]
