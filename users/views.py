"""Users views."""

# Django
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout, views as auth_views
from django.contrib.auth.decorators import login_required
from django.views.generic.detail import DetailView
from django.views.generic import FormView, UpdateView

from django.urls import reverse, reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin 

# Models
from django.contrib.auth.models import User
from users.models import Profile
from posts.models import Post

# Forms
from users.forms import ProfileForm, SignupForm

# Create your views here.

class PostDetailView(DetailView, LoginRequiredMixin):
    """ User's Posts detail view. """
    template_name = "users/post_detail.html"
    slug_fielf = 'id'
    slug_url_kwarg = 'id'

    queryset = Post.objects.all()
    context_object_name = 'post'

    def get_context_data(self, **kwargs):
        """ Add user's posts to context """
        context = super().get_context_data(**kwargs)
        post = self.get_object()
        #context["posts"] = Post.objects.filter(title=title)
        context["posts"] = Post.objects.get(id= post.id)
        return context
    

class UserDetailView(DetailView, LoginRequiredMixin):
    """ User detail view. """
    template_name = "users/detail.html"
    slug_field = 'username' # <- el username es un campo unico en nuestro modelo
    slug_url_kwarg = 'username' # <- [username] === users/urls.py /path/route/str:[username]
    # queryset : trae a todos los usuarios (Django concatena todas las peticiones (todos los usuarios)) para hacer el query más especiífico y Django busca el objeto ahí.
    queryset = User.objects.all()

    context_object_name = 'user'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # user
        user = self.get_object()
        # Query
        # from posts.models import Post
        context["posts"] = Post.objects.filter(user=user).order_by('-created')
        return context
    

class UpdateProfileView(UpdateView, LoginRequiredMixin):
    """User Update."""
    template_name = "users/update_profile.html"
    model = Profile
    fields = ['website', 'phone_number', 'biografy']
    #success_url = "users:detail"

    def get_object(self):
        """Return user's profile."""
        return self.request.user.profile
    
#    def form_valid(self, form):
        """If the form is invalid, render the invalid form."""
#        if form['phone_numbre']== 0:
#            return self.render_to_response(self.get_context_data(form=form))
#        else:
#            return 1
    
    def get_success_url(self):
        """Return to user's profile."""
        username = self.object.user.username
        return reverse_lazy('users:detail', kwargs={'username': username})


@login_required
def update_profile(request):
    """Update a users's profile view."""
    profile = request.user.profile

    if request.method == 'POST':
        # form is a instance of ProfileForm with request
        # request.FILES es utilizado para recibir los archivos, desde el FORM enctype="multipart/form-data"
        form = ProfileForm(request.POST, request.FILES)
        
        #print(form.cleaned_data)
        if form.is_valid():
            #print(form.cleaned_data)
            data = form.cleaned_data
            profile.website = data['website']
            profile.phone_number = data['phone_number']
            profile.biografy = data['biografy']
            #profile.picture = data['picture']
            #print("error------------------------------------------- {}".format(data['picture']))
            if data['picture'] == None:
                pass
            else:
                profile.picture = data['picture']
            profile.save()

            # Creacion de una url con kwargs: from django.urls import reverse
            url = reverse('users:detail', kwargs={'username': request.user.username})
            return redirect(url)

        else:
            print("NO++++++++++++++++++++++++++++++++++++++++++++++++++")
    else:
        form = ProfileForm()

    return render(
        request=request, 
        template_name='users/update_profile.html',
        context={
            'profile': profile,
            'user': request.user,
            'form': form
        }
    )


class LoginView(auth_views.LoginView):
    """Login view."""
    redirect_authenticated_user= True
    template_name = 'users/login.html'


class LogoutView(auth_views.LogoutView, LoginRequiredMixin):
    """Logout view."""
    # this template don't used.
    template_name = 'registration/logged_out.html'

class SignupView(auth_views.LoginView ,FormView):
    """Signup a user."""
    login_url = '/'
    redirect_field_name = 'posts:feed'
    redirect_authenticated_user = True


    template_name = 'users/signup.html'
    form_class = SignupForm
    success_url = reverse_lazy('users:login')

    def form_valid(self, form):
        """Save form data."""
        """Solucción básica:
        form.save()
        return super().form_valid(form)
        """
        """ Save form data and login new user."""
        form.save()

        username = form['username'].value()
        password = form['password'].value()

        user = authenticate(
            self.request,
            username = username,
            password = password
        )

        login(self.request, user)
        return super().form_valid(form)
