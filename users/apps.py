"""User app configuration."""
# Django
from django.apps import AppConfigi


class UsersConfig(AppConfig):
    """User app configuration."""
    name = 'users'
    verbose_name = 'Users'
