""" User forms. """

# Django
from django import forms
from django.core.exceptions import ValidationError

# Models
from django.contrib.auth.models import User
from users.models import Profile

#utilities
import re


class ProfileForm(forms.Form):
    """Profile form"""
    
    # website is required by Middleware
    website = forms.URLField(max_length=200, required=True)
    # biografy isn't required by Middleware
    biografy = forms.CharField(max_length=400, required=False)
    # phone_number isn't required by Middleware
    phone_number = forms.CharField(required=False)
    # picture is required by Middleware
    picture = forms.ImageField(required=False)


    def clean_phone_number(self):
        """The phone number must be in the following format: (123) 456-7890 or 123-4567."""
        phone_number = self.cleaned_data['phone_number']

        first_option = re.search("^[(][0-9]{3}[)][ ][0-9]{3}[-][0-9]{4}$", phone_number)
        second_option = re.search("^[0-9]{3}[-][0-9]{4}$", phone_number)
        if first_option or second_option:
            return phone_number
        else:
            raise forms.ValidationError('Passwords do not match. :(')



class SignupForm(forms.Form):
    """ Sign up form. """
    username = forms.CharField(
        min_length=4, 
        max_length=50,
        label=False,
        widget=forms.TextInput(attrs={'placeholder':'username','class': 'form-control','required': True}))
    password = forms.CharField(
        min_length=6, 
        max_length=100,
        label=False,
        widget=forms.PasswordInput(attrs={'placeholder':'Password (min: 6)','class':'form-control', 'required': True}))
    password_confirmation = forms.CharField(
        min_length=6, 
        max_length=100, 
        label=False,
        widget=forms.PasswordInput(attrs={'placeholder':'Password (min: 6)','class':'form-control', 'required': True}))
    first_name = forms.CharField(
        min_length=3, 
        max_length=50,
        label=False,
        widget=forms.TextInput(attrs={'placeholder':'First name','class': 'form-control','required': True}))
    last_name = forms.CharField(
        min_length=3, 
        max_length=50,
        label=False,
        widget=forms.TextInput(attrs={'placeholder':'Last name','class': 'form-control','required': True}))
    email = forms.CharField(
        min_length=6, 
        max_length=100, 
        label=False,
        widget=forms.EmailInput(attrs={'placeholder':'Email','class': 'form-control', 'required': True}))

    def clean_username(self):
        """ Username must be unique. """
        username = self.cleaned_data['username']
        username_taken = User.objects.filter(username=username).exists()
        if username_taken:
            raise forms.ValidationError('Username is already in use. :(')
        return username


    def clean(self):
        """ Verify password confirmation match. """
        data = super().clean()

        password = data['password']
        password_confirmation = data['password_confirmation']

        if password != password_confirmation:
            raise forms.ValidationError('Passwords do not match. :(')
        return data

    
    def save(self):
        """ Create user and profile. """
        data = self.cleaned_data
# password_confirmation isn't need
        data.pop('password_confirmation')

        user = User.objects.create_user(**data)
        profile = Profile(user=user)
        profile.save()


