"""platzigram URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
""" Platzigram URLs module """
# Utilities
import os

# Django
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

# Codigo facilito   -   no causo ningun cambio
from django.conf.urls import url


urlpatterns = [
    #  problema at reverse lo pueden solucionar añadiendo al path de platzigram/urls.py lo siguiente:
    #path(r'', include('django.contrib.auth.urls')),
    # url : admin
    path('admin/', admin.site.urls, name='admin'),
    # url : posts
    path('', include(('posts.urls', 'posts'), namespace='posts')),
    # url : users
    path('', include(('users.urls', 'users'), namespace='users')),

    #path('posts/', include('platzigram.posts.urls'))

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


#from platzigram import views as local_views

#path('hello-world/', local_views.hello_world, name='hello_world'),
#path('hi/', local_views.hi, name='hi'),
#path('op/', local_views.operaciones, name='operations'),
#path('sol/', local_views.solucion_profe, name='solution'),
#path('age/<int:age>/<str:name>', local_views.age, name='age'),
