"""Platigram views"""
# Module: Django
from django.http import HttpResponse
from django.http import JsonResponse
import json

# Module: Utilities
from datetime import datetime


def hello_world(request):
    """Return a greeting."""
    return HttpResponse(
        'Hello, world. I\'m Django.\n Current server time is {now}'.format(
            now = datetime.now().strftime(
                '%b %dth, %y - %H:%M:%S hrs'
            )
        )
    )


def hi_2(request):
    """Object request"""
    # Obtenemos la lista de números del dict.
    numbers = request.GET['numbers'].split(',')
    print('type[numbers -> request.GET[numbers]]: {}'.format(type(numbers)))
    my_list = []
    # Convertimos los números de la lista de STR a INT
    for number in numbers:
        my_list.append(int(number))
        #print(my_list)
    # Salida a consola de la lista ordenada
    print('List sorted: {}'.format(sorted(my_list)))
    # Convertimos la lista ordenada en formato JsonResponse
    response = JsonResponse(sorted(my_list),safe=False)
    return response


def hi_3(request):
    """Object request"""
    numbers = sorted([int(number) 
        for number in request.GET['numbers'].split(',')])
# Converint Python Objects to JSON using json.dumps()
    response = json.dumps({"numbers": numbers})
    print(response)
    return HttpResponse('<h2>{response}</h2>'.format(response=response))


def hi(request):
    """Object request - map"""
    numbers = map(int, request.GET['numbers'].split(','))
    return JsonResponse({"numbers" : sorted(numbers)}, json_dumps_params={'indent':4})


def operaciones(request):
    """Operaciones con los argumentos enviados en el request"""
    numbers = [int(number) for number in request.GET['numbers'].split(',')]
    op_results = {"suma_total": 0, "resta_total": 0, "multi_total": 1}
    for number in numbers:
        op_results["suma_total"] = op_results["suma_total"] + number
        op_results['resta_total'] -= number 
        op_results['multi_total'] *= number

    return HttpResponse('<h2>{}</h2>'.format(op_results))


def solucion_profe(request):
    """ Operations soluction """
    numbers = sorted([int(i) for i in request.GET['numbers'].split(',')])
#    sorted_numbers = sorted(numbers)
    data = {
        'status': 'ok',
        'numbers': numbers,
        'message': 'Integers sorted successfully.'
    }
#    import pdb; pdb.set_trace()
#    json.dumps(dict{}) -> Traduce un diccionario a Json
    return HttpResponse(
        json.dumps(data, indent=4), 
        content_type='application/json'
    )

def age(request,age,name):
    """Return a greeting"""
    if age < 12:
        message = 'Sorry {}, you are not allowed here. :('.format(name)
    else:
        message = 'Hello, {}! Welcome to Platzigram - Django!!!'.format(name)
    return HttpResponse(message)




