"""
Platzigram middleware catalog.

"""

# Django
from django.shortcuts import redirect, render
from django.urls import reverse
from django.contrib.auth import logout

class ProfileCompletionMiddleware:
    """
    Profile completion middleware.

    Ensure every user that is interacting with the platform
    have their profile picture and biography.
    """
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuracion and initialization.
    

    def __call__(self, request):
        # Code to be execute for each request before
        # the view (and later middleware) are called.

        # If the usesr is registered:
        if not request.user.is_anonymous:
            # TRY If the user dont have profile in DB (By super user), theb error in middleware
            if not request.user.is_staff:

                # Get info by ONE TO ONE from model.user    [user = user]
                profile = request.user.profile
                # If the user registered don't have a picture or biografy:
                if not profile.picture and not profile.biografy:
                    # if the current path is != abs path:
                    if request.path not in [reverse('users:update_profile'), reverse('users:logout')]:
                        return redirect('users:update_profile')

                
#                    print('no profile----------------------------------')

        response = self.get_response(request)
        return response

