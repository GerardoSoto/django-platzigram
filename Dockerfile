# platzigram - platzigram_server 1.0.1
FROM ubuntu:latest

MAINTAINER gerardo.8.soto@gmail.com

COPY requirements.txt /

RUN apt update -y && apt list --upgradable && apt install python3.6 -y && apt install python3.6-dev -y && apt install python3.6-distutils -y && apt install wget -y && apt install python3-pip -y && wget https://bootstrap.pypa.io/get-pip.py -O /home/get-pip.py && python3.6 /home/get-pip.py && rm /home/get-pip.py && python3 --version && pip3 --version && pip3 install -r /requirements.txt

#COPY ~Desktop/django-platzigram/platzigram/requirements.txt /requirements.txt
#RUN pip3 install Django==2.2.16 Pillow==2.9.0 pytz=2020.1 sqlparse==0.4.1

#RUN pip3 install -r /requirements.txt

CMD echo "Starting server... 5 sec." && sleep 5 && python3 /app/manage.py runserver 0.0.0.0:7000 


